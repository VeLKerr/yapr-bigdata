from pyspark import SparkContext, SparkConf
import re

config = SparkConf().setAppName("griboedov").setMaster("yarn-client")
spark_context = SparkContext(conf=config)

rdd = spark_context.textFile("/data/griboedov")
rdd2 = rdd.map(lambda x: re.sub(u"\\W+", " ", x.strip(), flags=re.U))
rdd3 = rdd2.flatMap(lambda x: x.split(" ")).filter(lambda x: len(x) >= 3 and unicode(x[0]).isupper() and unicode(x[1:]).islower())
rdd4 = rdd3.map(lambda x: (x,1))
rdd5 = rdd4.reduceByKey(lambda a, b: a + b).sortBy(lambda a: a[1], ascending=False)
#rdd5 = rdd4.groupByKey()
#rdd6 = rdd5.map(lambda (k, v): (k, sum(v)))
words_count = rdd5.take(10)

for word, count in words_count:
    print word.encode("utf8"), count
