from pyspark import SparkContext, SparkConf
import re

config = SparkConf().setAppName("griboedov").setMaster("yarn-client")
spark_context = SparkContext(conf=config)

num = spark_context.accumulator(0)

rdd = spark_context.textFile("/data/griboedov")
rdd2 = rdd.map(lambda x: re.sub(u"\\W+", " ", x.strip(), flags=re.U))
rdd3 = rdd2.flatMap(lambda x: x.split(" ")).filter(lambda x: len(x) >= 3)

def count_upper(wrd):
    global num
    if unicode(wrd[0]).isupper() and unicode(wrd[1:]).islower():
        num += 1

rdd3.foreach(lambda x: count_upper(x))
print num.value
