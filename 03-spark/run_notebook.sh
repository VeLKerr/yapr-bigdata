#! /usr/bin/env bash

PYSPARK_DRIVER_PYTHON=jupyter PYSPARK_PYTHON=/usr/bin/python3 PYSPARK_DRIVER_PYTHON_OPTS='notebook --ip="*" --port=29999 --no-browser' pyspark --master=yarn --num-executors=2
