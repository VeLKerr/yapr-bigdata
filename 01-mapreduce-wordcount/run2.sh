#! /usr/bin/env bash

OUT_DIR="wordcount_result"

hdfs dfs -rm -r -skipTrash ${OUT_DIR}

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapred.job.name="Streaming wordCount" \
    -files mapper2.py,reducer.py \
    -mapper "python3 mapper2.py" \
    -reducer "python3 reducer.py" \
    -input "/data/silmarillion" \
    -output ${OUT_DIR} > /dev/null

hdfs dfs -ls ${OUT_DIR}
hdfs dfs -cat ${OUT_DIR}/part-0000* | sort -k2nr
