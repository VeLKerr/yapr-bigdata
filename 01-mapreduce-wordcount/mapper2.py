import sys
import re

sys.stdin = open(sys.stdin.fileno(), encoding='utf-8')
sys.stdout = open(sys.stdout.fileno(), 'w', encoding='utf-8')
stop_words = ["а", "и", "в", "не", "на", "с", "из"]
my_name = 'олег'

for line in sys.stdin:
    words = line.strip().split()
    for word in words:
        if word not in stop_words and not word.startswith(my_name[0]):
            print("{}\t{}".format(word.lower(), 1))
