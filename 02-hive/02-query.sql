USE yaprofi;

SET mapred.job.name=hive_task;
--EXPLAIN
SELECT AVG(counts.cnt)
FROM (
    SELECT mask, count(ip) as cnt
    FROM Subnets
    GROUP BY mask
) counts;
